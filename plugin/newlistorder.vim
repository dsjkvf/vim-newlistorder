
" HEADER
" Description:  vim-newlistorder plugin file
" Author:       dsjkvf <dsjkvf@gmail.com>
" Maintainer:   dsjkvf <dsjkvf@gmail.com>
" Notes:        born from this collection of delusions: https://www.reddit.com/c3umhn/


" INIT

" Check
if exists('g:newlistorder_loaded') || v:version < 700 || &cp
    finish
endif

" Set
let g:newlistorder_loaded = 1
if !exists('g:newlistorder_default_mappings')
    let g:newlistorder_default_mappings = 1
endif
let s:save_cpo = &cpo
set cpo&vim


" COMMANDS

" Order
command! -bang -range Order <line1>,<line2>call newlistorder#NewListOrder(<bang>0)
" Clear
command! -range OrderClear <line1>,<line2>s/^\(\s*\)[a-z]*[0-9*-.)]\+ /\1/


" MAPPINGS

" Order
if mapcheck("<Leader>x") == '' && g:newlistorder_default_mappings == 1
    nnoremap <Leader>x vip:Order
    xnoremap <Leader>x :Order
endif

" Clear
if mapcheck("<Leader>X") == '' && g:newlistorder_default_mappings == 1
    nnoremap <silent> <Leader>X vip:OrderClear<CR><C-l>
    xnoremap <silent> <Leader>X :OrderClear<CR><C-l>
endif


" FINI

" Restore
let &cpo = s:save_cpo
unlet s:save_cpo
