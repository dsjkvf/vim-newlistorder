
" HEADER
" Description:  vim-newlistorder autoload file
" Author:       dsjkvf <dsjkvf@gmail.com>
" Maintainer:   dsjkvf <dsjkvf@gmail.com>


" MAIN

" The only function
function! newlistorder#NewListOrder(bang) range
    " ask for parameters
    if a:bang
        let max_indent = input('Max indent? ')
        let min_indent = input('Min indent? ')
        let use_letters = input('Use letters? ')
        let suffix = input('New suffix? ')
    endif
    " clear the display after user input
    call feedkeys("\<C-l>")
    " set parameters
    if !exists('max_indent') || max_indent == ''
        let max_indent = 100
    else
        let max_indent = max_indent * &shiftwidth
    endif
    if !exists('min_indent') || min_indent == ''
        let min_indent = 0
    else
        let min_indent = min_indent * &shiftwidth
    endif
    if exists('use_letters') && use_letters =~? '^[a-hj-z]$'
        let letters = {
                    \ "1":"a", "2":"b", "3":"c", "4":"d", "5":"e",
                    \ "6":"f", "7":"g", "8":"h", "9":"i", "10":"j",
                    \ "11":"k", "12":"l", "13":"m", "14":"n", "15":"o",
                    \ "16":"p", "17":"q", "18":"r", "19":"s", "20":"t",
                    \ "21":"u", "22":"v", "23":"w", "24":"x", "25":"y",
                    \ "26":"z",
                    \ }
    elseif exists('use_letters') && use_letters =~? 'i'
        let letters = {
                    \ "1":"i", "2":"ii", "3":"iii", "4":"iv", "5":"v",
                    \ "6":"vi", "7":"vii", "8":"viii", "9":"ix", "10":"x",
                    \ }
    elseif exists('use_letters') && use_letters != ''
        let letters = {}
        for i in range(1, 100)
            let letters[string(i)] = use_letters
        endfor
    endif
    if !exists('suffix') || suffix == ''
        let suffix = '. '
    endif
    if suffix[-1:-1] != ' '
        let suffix = suffix . ' '
    endif
    " collect a dictionary of lines (keys) and their indents (values)
    let b:ordr_lines = {}
    for line in range(a:firstline, a:lastline)
        " skip empty lines
        if len(getline(line)) == 0
            continue
        endif
        if min_indent <= indent(line) && indent(line) <= max_indent
            let b:ordr_lines[string(line)] = indent(line)
        endif
    endfor
    " collect a dictionary of indents (keys) and their indexes (values)
    let b:order_indents = {}
    for line in range(a:firstline, a:lastline)
        " skip empty lines
        if len(getline(line)) == 0
            continue
        endif
       if index(keys(b:order_indents), string(indent(line))) < 0
           " set the first time indent to 1...
            let b:order_indents[string(indent(line))] = 1
        else
            " ...and increase it afterwards by 1
            let b:order_indents[string(indent(line))] += 1
        endif
        try
            " however, if the current line's indent is bigger than the previous line's indent,
            " i.e., the text is indented by more spaces/tabs, set this line's indent to 1
            " (this means that we are entering another sub-section, and the indexes' counter should be reset,
            " while if the current line's indent is lesser than the previous line's indent,
            " that would mean that we are returning to the previous sub-section, and the counter should continue)
            if index(keys(b:ordr_lines), string(line - 1)) > -1 && b:ordr_lines[string(line)] > b:ordr_lines[string(line - 1)]
                let b:order_indents[string(indent(line))] = 1
            endif
            " because of the provided parameters, choose the 'alphabet' for the indexes
            if exists('use_letters') && use_letters != ''
                let sign = letters[b:order_indents[string(indent(line))]]
            else
                let sign = b:order_indents[string(indent(line))]
            endif
            " detect, for the current line, for how many spaces exactly are we looking
            let spaces = b:ordr_lines[string(line)]
            " ...or tabs
            let tabs = b:ordr_lines[string(line)] / &shiftwidth
            " and finallly, by using all the previously collected information live, add an index to the current line
            execute line . 's/^\(\t\{' . tabs . '}\|\s\{' .  spaces . '}\)/\1' . sign . suffix . '/'
        catch
        endtry
    endfor
    " clear dictionaries
    unlet b:ordr_lines
    unlet b:order_indents
endfunction
