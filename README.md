New List Order
==============

## About

This is a plugin for ordering lists by numbering their items (or by inserting other possible indexes, like letters, Roman numbers, or any other sort of markers):

<p style="text-align: center;"><img src="https://i.imgur.com/iOdPOrM.png" align="middle">      ->      <img src="https://i.imgur.com/d4Vpe7P.png" align="middle"></p>


## Commands and mappings

The plugin introduces two commands, `Order[!]` and `OrderClear`, which, if these keybindings are available, are mapped to `<Leader>x` and `<Leader>X`. However, the default mappings may be disabled by setting `g:newlistorder_default_mappings` to `0`.

If launched with a bang, `Order` command accepts the following options:

  - Max indent: process those list items, which are indented no more than N times (default: 100)
  - Min indent: process those list items, which are indented no less than N times (default: 0)
  - Use letters: enter `i` or `I` to use Roman numbers, or any other English letter to use the English alphabet (default: numbers)
  - New suffix: characters to add after the marker (default: `. `)

## Installation

Copy the provided files to your `$VIMHOME` directory, or simply use any plugin manager available.

## Demonstration

![gif](https://i.imgur.com/qEKd4Md.gif)

## License

Copyright (c) dsjkvf unless stated otherwise. Distributed under the same terms as Vim itself. See `:help license`.
